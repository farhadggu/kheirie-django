from django.urls import path
from .views import home, detail, posts

app_name = "home"
urlpatterns = [
    path('', home, name="home"),
    path('detail/<slug:slug>', detail, name="detail"),
    path('posts', posts, name="posts")
]