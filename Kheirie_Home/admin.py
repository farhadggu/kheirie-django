from django.contrib import admin
from embed_video.admin import AdminVideoMixin
from .models import Post


class PostAdmin(AdminVideoMixin, admin.ModelAdmin):
    list_display = ('title', 'slug', 'publish', 'status')
    list_filter = ('publish', 'status')
    search_fields = ('title', 'description')
    prepopulated_fields = {"slug": ('title',)}
    ordering = ['-status', '-publish']


admin.site.register(Post, PostAdmin)

