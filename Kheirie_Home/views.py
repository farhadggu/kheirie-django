from django.shortcuts import render, get_object_or_404
from .models import Post


def home(request):
    context = {
        "post": Post.objects.filter(status="p").order_by("-publish")[:3]
    }
    return render(request, 'home.html', context)


def detail(request, slug):
    context = {
        "detail": get_object_or_404(Post, slug=slug, status="p")
    }
    return render(request, 'detail.html', context)


def posts(request):
    context = {
        "post": Post.objects.filter(status="p").order_by("-publish")
    }
    return render(request, 'posts.html', context)