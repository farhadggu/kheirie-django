# Generated by Django 3.1.6 on 2021-02-06 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Kheirie_Home', '0002_auto_20210206_1618'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='thumbnail',
            field=models.ImageField(default=0, upload_to='images', verbose_name='تصویر'),
            preserve_default=False,
        ),
    ]
