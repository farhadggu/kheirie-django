from django.apps import AppConfig


class KheirieHomeConfig(AppConfig):
    name = 'Kheirie_Home'
