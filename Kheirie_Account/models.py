from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    file = models.FileField(upload_to='documents', verbose_name="آپلود فایل")


