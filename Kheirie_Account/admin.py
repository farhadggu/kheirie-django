from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User

UserAdmin.fieldsets[2][1]['fields'] = ('file', 'is_active', 'is_staff', 'is_superuser'),

admin.site.register(User, UserAdmin)

