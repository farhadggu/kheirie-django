from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from Kheirie_Home.models import Post
from django.urls import reverse_lazy
from .forms import SignupForm
from .models import User


class PostLists(LoginRequiredMixin, ListView):
    template_name = 'registration/home.html'

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Post.objects.all()
        else:
            return Post.objects.filter()


class PostCreate(LoginRequiredMixin, CreateView):
    model = Post
    fields = ["title", "slug", "description", "thumbnail", "publish", "status"]
    template_name = "registration/post-create-update.html"


class PostUpdate(LoginRequiredMixin, UpdateView):
    model = Post
    fields = ["title", "slug", "description", "thumbnail", "publish", "status"]
    template_name = "registration/post-create-update.html"


class PostDelete(DeleteView):
    model = Post
    success_url = reverse_lazy('account:home')
    template_name = "registration/post-confirm-delete.html"


class Register(CreateView):
    form_class = SignupForm
    template_name = "registration/register.html"

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = True
        user.save()
        return redirect("/account/login")


class Profile(UpdateView):
    model = User
    fields = ['username', 'email', 'first_name', 'last_name', 'file']
    template_name = "registration/profile.html"
    success_url = reverse_lazy("account:profile")

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)

