from django.apps import AppConfig


class KheirieAccountConfig(AppConfig):
    name = 'Kheirie_Account'
    verbose_name="حساب"
