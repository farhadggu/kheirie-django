from django.contrib.auth import views
from django.urls import path
from .views import PostLists, PostCreate, PostUpdate, PostDelete, Register, Profile

app_name = "account"
urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),

    path('logout/', views.LogoutView.as_view(), name='logout'),
    #
    # path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    # path('password_change/done/', views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    #
    # path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    # path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    # path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    # path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]

urlpatterns += {
    path('', PostLists.as_view(), name="home"),
    path('post/create', PostCreate.as_view(), name="post-create"),
    path('post/update/<int:pk>', PostUpdate.as_view(), name="post-update"),
    path('post/delete/<int:pk>', PostDelete.as_view(), name="post-delete"),
    path('signup', Register.as_view(), name="signup"),
    path('profile/', Profile.as_view(), name="profile"),

}